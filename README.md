### Praktikum 3 - Software Engineering 2

- Name: **Andr� Kuhlmann**
- Matr.-Nr:	**779690**
- Erstellt: **30. Juni 2020**
- Java: **14**
- IDE: **[IntelliJ IDEA](https://www.jetbrains.com/de-de/idea/)**

#### Themen:
- Einsatz von Exceptions
- Permanente Speicherung �ber eine mysql-Datenbank

#### Aufgaben:  
1. Kopieren Sie das Praktikum 2  
2. Speichern Sie die Informationsobjekte ihres Projektes in einer Datenbank und lesen Sie die Objekte bei einem erneuten Start wieder ein.
3. Verwenden Sie Exceptions zur Signalisierung von Ausnahmesitutationen, z.B. Datenbank kann nicht ge�ffnet werden.
4. Geben Sie den Text der Exceptions in der Benuteroberfl�che oder der Konsole aus.