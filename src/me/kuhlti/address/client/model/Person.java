package me.kuhlti.address.client.model;

import java.time.LocalDate;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

import me.kuhlti.address.client.generated.SPerson;

public class Person {

    // ====== Properties ======

    private final String id;
    private final StringProperty firstName;
    private final StringProperty lastName;
    private final StringProperty city;
    private final StringProperty street;
    private final IntegerProperty postalCode;
    private final ObjectProperty<LocalDate> birthday;


    // ====== Initializer ======

    public Person(String firstName, String lastName, String city, String street, Integer postalCode, LocalDate birthday) {
        this.id         = UUID.randomUUID().toString();

        this.firstName  = new SimpleStringProperty(firstName);
        this.lastName   = new SimpleStringProperty(lastName);
        this.city       = new SimpleStringProperty(city);
        this.street     = new SimpleStringProperty(street);
        this.postalCode = new SimpleIntegerProperty(postalCode);
        this.birthday   = new SimpleObjectProperty<LocalDate>(birthday);
    }

    public Person(SPerson sPerson) {
        this.id         = sPerson.getID();
        this.firstName  = new SimpleStringProperty(sPerson.getFirstName());
        this.lastName   = new SimpleStringProperty(sPerson.getLastName());
        this.city       = new SimpleStringProperty(sPerson.getCity());
        this.street     = new SimpleStringProperty(sPerson.getStreet());
        this.postalCode = new SimpleIntegerProperty(sPerson.getPostalCode());
        this.birthday   = new SimpleObjectProperty<LocalDate>(LocalDate.parse(sPerson.getBirthday())); // WARNING: May brakes when string invalid
    }

    public static Person emptyPerson() {
        return new Person("", "", "", "", 0, LocalDate.now());
    }

    // ====== Functions ======

    /**
     * Get's an random element from the provided array
     * @param list The list of items from which to pick
     * @param <T> Can be used on any object type -> generic
     * @return Random element of type T
     */
    private static <T> T getRandomElementFrom(List<T> list) {
        Random rand = new Random();
        return list.get(rand.nextInt(list.size()));
    }

    /**
     * Converts the Person to a Server SPerson.
     * @return
     */
    public SPerson toSPerson() {
        SPerson sPerson = new SPerson();

        sPerson.setID(getID());
        sPerson.setFirstName(getFirstName());
        sPerson.setLastName(getLastName());
        sPerson.setCity(getCity());
        sPerson.setStreet(getStreet());
        sPerson.setPostalCode(getPostalCode());
        sPerson.setBirthday(getBirthday().toString());

        return sPerson;
    }


    // ====== Functions ======

    public StringProperty firstNameProperty() {
        return firstName;
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }

    public String fullName() { return firstName + " " + lastName; }


    // ====== Getters ======

    public String getID() { return id; }

    public String getFirstName() {
        return firstName.get();
    }

    public String getLastName() {
        return lastName.get();
    }

    public String getCity() {
        return city.get();
    }

    public String getStreet() {
        return street.get();
    }

    public Integer getPostalCode() {
        return postalCode.get();
    }

    public LocalDate getBirthday() {
        return birthday.get();
    }


    // ====== Setters ======

    public void setFirstName(String value) {
        firstName.set(value);
    }

    public void setLastName(String value) {
        lastName.set(value);
    }

    public void setStreet(String value) {
        street.set(value);
    }

    public void setCity(String value) {
        city.set(value);
    }

    public void setPostalCode(int value) {
        postalCode.set(value);
    }

    public void setBirthday(LocalDate value) {
        birthday.set(value);
    }

}
