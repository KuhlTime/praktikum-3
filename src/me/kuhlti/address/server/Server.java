package me.kuhlti.address.server;

import me.kuhlti.address.server.model.SPerson;
import me.kuhlti.address.server.model.DBManager;

import javax.jws.*;
import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;

@WebService(name = "SPersonAdminWS")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class Server {

    // ====== WebMethods ======

    @WebMethod public SPerson[] getAll() {
        ArrayList<SPerson> persons = DBManager.queryAll();
        SPerson[] array = new SPerson[persons.size()];
        return persons.toArray(array);
    }

    @WebMethod public void add(SPerson sPerson) {
        DBManager.insert(sPerson);
    }

    @WebMethod public void update(SPerson sPerson) {
        DBManager.update(sPerson);
        System.out.println("Updated: " + sPerson.fullName());
    }

    @WebMethod public void delete(SPerson sPerson) {
        DBManager.delete(sPerson);
        System.out.println("Deleted: " + sPerson.fullName());
    }
}
