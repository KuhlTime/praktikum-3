package me.kuhlti.address.server.model;

import java.sql.*;
import java.util.ArrayList;

public class DBManager {

    // ====== Parameters ======

    private static final String IP = "192.168.178.22";
    private static final String PORT = "3306";
    private static final String TABLE = "persons";
    private static final String DB = "address";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "1234";


    // ====== Fields ======

    // TODO: Remove or replace with something more useful
    public enum Field {
        id, firstName, lastName, city, street, postalCode, birthday;

        /**
         * Returns the respective database label for each case.
         * @return
         */
        public String dbLabel() {
            return switch (this) {
                case id -> "id";
                case firstName -> "firstName";
                case lastName -> "lastName";
                case city -> "city";
                case street -> "street";
                case postalCode -> "postalCode";
                case birthday -> "birthday";
            };
        }
    }


    // ====== Fetch all Persons from DB =====

    public static ArrayList<SPerson> queryAll() {
        return queryAll(null, null);
    }

    public static ArrayList<SPerson> queryAll(String select, String where) {
        ArrayList<SPerson> persons = new ArrayList<SPerson>();

        try {
            Connection conn = establishDBConnection();

            Statement stmt = conn.createStatement();
            ResultSet rs;

            select = (select == null) ? "*" : select;
            String statement = "SELECT " + select + " FROM " + TABLE;
            if (where != null) {
                statement += " WHERE " + where;
            }

            rs = stmt.executeQuery(statement);

            // TODO: Hard to read. Please replace
            while (rs.next()) {
                SPerson p = new SPerson();

                p.setID(rs.getString(Field.id.dbLabel()));
                p.setFirstName(rs.getString(Field.firstName.dbLabel()));
                p.setLastName(rs.getString(Field.lastName.dbLabel()));
                p.setCity(rs.getString(Field.city.dbLabel()));
                p.setStreet(rs.getString(Field.street.dbLabel()));
                p.setPostalCode(rs.getInt(Field.postalCode.dbLabel()));
                p.setBirthday(rs.getString(Field.birthday.dbLabel()));

                persons.add(p);
            }

            rs.close();
        } catch (Exception e) {
            System.err.println("Error in queryAll(): " + e.getMessage());
        }

        return persons;
    }


    // ====== Add new Person to DB =====

    public static void insert(SPerson person) {

        if (checkExists(person)) {
            System.err.println("Error Adding: Already found a person with the same ID " + person.getID());
            return;
        }

        // CAREFULL: Order is Key!!!
        // TODO: Would be saver to use a HASHMAP

        String query = "INSERT INTO " + TABLE
                + " (id, firstName, lastName, city, street, postalCode, birthday)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?)";


        try {
            Connection conn = establishDBConnection();
            PreparedStatement stmt = conn.prepareStatement(query);

            stmt.setString(1, person.getID());
            stmt.setString(2, person.getFirstName());
            stmt.setString(3, person.getLastName());
            stmt.setString(4, person.getCity());
            stmt.setString(5, person.getStreet());
            stmt.setInt(6, person.getPostalCode());
            stmt.setString(7, person.getBirthday());

            stmt.executeUpdate();
            conn.close();

            System.out.println("Added: " + person.fullName());
        } catch (Exception e) {
            System.err.println("Error in insert(): " + e.getMessage());
        }
    }


    // ====== Update Person in DB =====

    public static void update(SPerson person) {
        if (!checkExists(person)) {
            System.err.println("Error Updating: Found no person with " + person.getID());
            return;
        }

        String query = "UPDATE " + TABLE + " SET "
                + "firstName = ?, "
                + "lastName = ?, "
                + "city = ?, "
                + "street = ?, "
                + "postalCode = ?, "
                + "birthday = ?"
                + "WHERE id = ?";

        try {
            Connection conn = establishDBConnection();
            PreparedStatement stmt = conn.prepareStatement(query);

            stmt.setString(1, person.getFirstName());
            stmt.setString(2, person.getLastName());
            stmt.setString(3, person.getCity());
            stmt.setString(4, person.getStreet());
            stmt.setInt(5, person.getPostalCode());
            stmt.setString(6, person.getBirthday());
            stmt.setString(7, person.getID());

            stmt.executeUpdate();
            stmt.close();

        } catch (Exception e) {
            System.err.println("Error in update(): " + e.getMessage());
        }
    }

    public static void delete(SPerson person) {
        delete(person.getID());
    }


    // ====== Delete Person in DB =====

    public static void delete(String id) {
        if (!checkExists(id)) {
            System.err.println("Error Deleting: Found no person with ID " + id);
            return;
        }

        String query = "DELETE FROM persons WHERE id = '" + id + "'";

        try {
            Connection conn = establishDBConnection();
            Statement stmt = conn.createStatement();

            stmt.executeUpdate(query);
            stmt.close();

        } catch (Exception e) {
            System.err.println("Error in delete(): " + e.getMessage());
        }
    }


    // ====== Check if Person exists in DB =====

    public static boolean checkExists(SPerson person) {
        return checkExists(person.getID());
    }

    public static boolean checkExists(String id) {

        String query = "SELECT COUNT(*) AS row_count FROM persons WHERE persons.id = '" + id + "'";

        int count = 0;

        try {
            Connection conn = establishDBConnection();
            Statement stmt = conn.createStatement();
            ResultSet rs;

            rs = stmt.executeQuery(query);
            rs.next();

            count = rs.getInt("row_count");

            rs.close();

        } catch (Exception e) {
            System.err.println("Error in checkExists(): " + e.getMessage());
        }

        return (count != 0);
    }


    // ====== Helpers ======

    private static Connection establishDBConnection() throws Exception {
        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://" + IP + ":" + PORT + "/" + DB;

        // Fix Time Zone Issue
        url += "?useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

        return DriverManager.getConnection(url, USERNAME, PASSWORD);
    }

}
