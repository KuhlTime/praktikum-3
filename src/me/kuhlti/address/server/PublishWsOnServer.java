package me.kuhlti.address.server;

import javax.swing.JOptionPane;
import javax.xml.ws.Endpoint;

public class PublishWsOnServer {

	public static void main(String[] args) {
		String url = ADDRESS("192.168.178.25", "8080");

		Endpoint endpoint = Endpoint.publish(url, new Server());
		System.out.println("Started server on: " + url);

		JOptionPane.showMessageDialog(null, "Server beenden");
		endpoint.stop();
	}

	private static String ADDRESS(String IP, String PORT) {
		return "http://" + IP + ":" + PORT + "/service";
	}
}